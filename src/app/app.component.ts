import { Component } from '@angular/core';
import {AuthService} from "./services/auth.service";
import {NavbarItem} from "./navbar/navbar-item";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'exchange';
  isMenuOpen: boolean = false

  public isAuth: boolean = false;

  public navbarItems: NavbarItem[] = [
    {
      title: 'Главная',
      icon: 'home',
      route: ''
    },
    {
      title: 'Обмен',
      icon: 'swap',
      route: 'swap',
      disabled: true,
    },
    {
      title: 'Депозит',
      icon: 'deposit',
      route: 'deposit'
    },
    {
      title: 'Реквизиты',
      icon: 'replenishment',
      route: 'replenishment'
    },
    {
      title: 'История',
      icon: 'bookmark',
      route: 'history',
      disabled: true,
    },
  ];

  constructor(
    private readonly authService: AuthService,
  ) {
    this.authService.$authInfo.subscribe(authInfo => {
      if(authInfo) {
        this.isAuth = true;
        return;
      }
      this.isAuth = false;
    });
  }

  public async logIn(): Promise<void> {
    await this.authService.logIn();
  }

  public async logOut(): Promise<void> {
    await this.authService.logOut();
  }

}

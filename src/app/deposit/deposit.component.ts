import {Component} from '@angular/core';
import {Lending} from "../model/lending/lending";
import {AuthService} from "../services/auth.service";
import {LendingService} from "../services/lending.service";
import {CurrencyConverterService} from "../services/currency-converter.service";
import {LendingStatus} from "../model/lending/lending-status";

@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.scss']
})
export class DepositComponent {
  public selectedDepositFilter: LendingStatus = LendingStatus.IN_PROGRESS
  public depositsFilter = [
    { id: LendingStatus.IN_PROGRESS, name: 'Открытые депозиты' },
    { id: LendingStatus.CLOSED, name: 'Закрытые депозиты' },
    { id: LendingStatus.RETURNED, name: 'Возвращенные депозиты' },
  ]
  public deposits: Lending[] = [];

  constructor(
    private readonly authService: AuthService,
    private readonly lendingService: LendingService,
    private readonly currencyConverterService: CurrencyConverterService
  ) {
    this.authService.$authInfo.subscribe(async authInfo => {
      if(!authInfo) {
        return;
      }

      await this.lendingService.fetchUserLending(authInfo.id);
    });

    this.lendingService.$userLending.subscribe(lending => {
      this.deposits = lending.filter(lending => lending.status === this.selectedDepositFilter);
    });
  }

  public getStatusTitle(lending: Lending) {
    if(lending.inProgress()) {
      return 'Открыт';
    }
    if(lending.isClosed()) {
      return 'Закрыт'
    }
    if(lending.isReturned()) {
      return 'Возвращен';
    }

    return '';
  }

  public async filterLending(): Promise<void> {
    await this.lendingService.$userLending.next(this.lendingService.$userLending.getValue());
  }

  public async returnLending(lending: Lending): Promise<void> {
    await this.lendingService.returnLending(lending, this.authService.$authInfo.getValue()?.id as string);
  }

  public async convertAmount(amount: bigint): Promise<number> {
    return await this.currencyConverterService.convert('LDS', 'RUB', amount);
  }
}

import { Component, OnInit } from '@angular/core';
import {AccountService} from "../services/account.service";

@Component({
  selector: 'app-top-up',
  templateUrl: './top-up.component.html',
  styleUrls: ['./top-up.component.scss']
})
export class TopUpComponent implements OnInit {
  public walletId = ''
  public qrLink = 'https://chart.googleapis.com/chart?cht=qr&chs=400x400&chl=false';

  constructor(
    private readonly accountService: AccountService,
  ) {
    this.accountService.$userAccount.subscribe(account => {
      if(account)  {
        this.walletId = account.address;
        this.qrLink = this.getQrLink();
      }
    })
  }

  ngOnInit(): void {
  }

  public async copyToClipBoard(): Promise<void> {
    await window.navigator.clipboard.writeText(this.walletId);
  }

  public getQrLink(): string {
    return `https://chart.googleapis.com/chart?cht=qr&chs=400x400&chl=${this.walletId}`;
  }
}

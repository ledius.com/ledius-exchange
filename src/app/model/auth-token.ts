import {Type} from "class-transformer";

export class AuthToken {
  public readonly accessToken!: string;
  public readonly refreshToken!: string;
  public readonly type!: "bearer";

  @Type(() => Date)
  public readonly expiresIn!: Date;
}

import {Account} from "../account/account";
import {Transform, Type} from "class-transformer";
import {LendingStatus} from "./lending-status";

export class Lending {
  public readonly id!: string;

  @Type(() => Account)
  public readonly account!: Account;

  @Transform(({value}) => {
    return BigInt(value);
  })
  public readonly amount!: bigint;

  @Transform(({value}) => {
    return BigInt(value);
  })
  public readonly interestsAmount!: bigint;

  @Type(() => Date)
  public readonly lastInterestsAt!: Date;

  public readonly status!: LendingStatus;

  @Type(() => Date)
  public readonly endDate!: Date;

  @Type(() => Date)
  public readonly createdAt!: Date;

  private format(amount: bigint): string {
    const balance = String(amount);
    const decimals = 18;

    const [scale, precision] = [
      BigInt(balance) / BigInt(10 ** decimals),
      balance
        .split('')
        .slice(-18)
        .join('')
        .padStart(decimals, '0')
    ];
    return `${scale}.${precision}`;
  }

  public formattedAmount(): string {
    return this.format(this.amount)
  }

  public formattedInterestsAmount(): string {
    return this.format(this.interestsAmount);
  }

  public percentProfit(): string {
    if(this.interestsAmount <= 0 || this.interestsAmount <= BigInt(0)) {
      return '0';
    }
    return String(BigInt(this.interestsAmount) * BigInt(100) / BigInt(this.amount))
  }

  public inProgress(): boolean {
    return this.status === LendingStatus.IN_PROGRESS;
  }

  public isClosed(): boolean {
    return this.status === LendingStatus.CLOSED;
  }

  public isReturned(): boolean {
    return this.status === LendingStatus.RETURNED;
  }

  public summaryProfit(): bigint {
    return this.interestsAmount + this.amount;
  }

  public formattedSummaryProfit(): string {
    return this.format(this.summaryProfit());
  }
}

import { Transform } from "class-transformer";
import {FormattedAmount} from "../formatted-amount";

export class Balance {
  @Transform(({value}) => {
    return BigInt(value);
  })
  public readonly balance: bigint;

  public readonly symbol: string;

  constructor(balance: bigint, symbol: string) {
    this.balance = balance;
    this.symbol = symbol;
  }

  public isLDS() {
    return this.symbol === 'LDS';
  }

  public isBNB () {
    return this.symbol === 'BNB';
  }

  public toFixed(): number {
    return Number(
      this.balance / BigInt(10 ** 18),
    );
  }

  public static empty(symbol: string): Balance {
    return new Balance(BigInt(0), symbol);
  }

  public formattedBalance(length: number = 18): string {
    return new FormattedAmount(this.balance).format(length);
  }
}

export class FormattedAmount {
  constructor(
      private readonly amount: bigint
  ) {}

  public format(length: number = 18, decimals: number = 18): string {

    const balance = String(this.amount);

    const [scale, precision] = [
      BigInt(balance) / BigInt(10 ** decimals),
      balance
        .slice(-decimals)
        .padStart(decimals, '0')
        .slice(0, length)
    ];
    return `${scale}.${precision}`;
  }
}

export class Pagination {
  public readonly order?: string;
  public readonly orderBy?: string;
  public readonly page: number = 0;
  public readonly pageSize: number = 5;
  public readonly total: number = 0;

  public constructor(
    page: number = 0,
    pageSize: number = 5,
    order?: string,
    orderBy?: string,
    total?: number
  ) {
    this.page = page;
    this.pageSize = pageSize;
    this.order = order;
    this.orderBy = orderBy;
    this.total = total ?? 0;
  }

  public static empty(): Pagination {
    return new Pagination();
  }

  public static default(): Pagination {
    return new Pagination(0, 5);
  }

  public get totalPages(): number {
    return Math.ceil(this.total / this.pageSize);
  }

  public nextPage(): Pagination {
    return new Pagination(this.page + 1, this.pageSize, this.order, this.orderBy, this.total);
  }

  public prevPage(): Pagination {
    if(this.page <= 0) {
      return new Pagination(0, this.pageSize, this.order, this.orderBy, this.total);
    }
    return new Pagination(this.page - 1, this.pageSize, this.order, this.orderBy, this.total);
  }

  public toQueryString(): string {
    const searchParams = new URLSearchParams();

    if(Number.isFinite(this.page)) {
      searchParams.set('pagination[page]', this.page!.toString());
    }
    if(Number.isFinite(this.pageSize)) {
      searchParams.set('pagination[pageSize]', this.pageSize!.toString());
    }
    if(this.orderBy) {
      searchParams.set('pagination[orderBy]', this.orderBy);
    }
    if(this.order) {
      searchParams.set('pagination[order]', this.order);
    }

    return searchParams.toString();
  }

  public toObject(): Record<string, string> {
    const obj = {} as Record<string, string>;

    if(Number.isFinite(this.page)) {
      obj['pagination[page]'] = this.page!.toString();
    }
    if(Number.isFinite(this.pageSize)) {
      obj['pagination[pageSize]'] = this.pageSize!.toString();
    }
    if(this.orderBy) {
      obj['pagination[orderBy]'] = this.orderBy;
    }
    if(this.order) {
      obj['pagination[order]'] = this.order;
    }

    return obj;
  }
}

import {Account} from "../account/account";
import {BankDetails} from "../bank-details/bank-details";
import {Transform, Type} from "class-transformer";
import {OfferStatus} from "./offer-status";
import {OfferType} from "./offer-type";
import {FormattedAmount} from "../formatted-amount";

export class Offer {
  public readonly id!: string;

  @Transform(({value}) => {
    return BigInt(value);
  })
  public readonly amount!: bigint;

  public readonly rate!: { currency: string, price: number };
  public readonly status!: OfferStatus;
  public readonly type!: OfferType;

  @Type(() => Account)
  public readonly owner!: Account;

  @Type(() => Account)
  public readonly reservedBy?: Account;

  @Type(() => Date)
  public readonly reservedAt?: Date;

  @Type(() => BankDetails)
  public readonly bankDetails?: BankDetails;

  @Type(() => Date)
  public readonly publishedAt!: Date;

  @Type(() => Date)
  public readonly createdAt!: Date;

  @Type(() => Date)
  public readonly updatedAt!: Date;

  public get formattedAmount(): string {
    return new FormattedAmount(this.amount).format(9);
  }
}

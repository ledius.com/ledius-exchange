export enum OfferStatus {
  OPENED,
  RESERVED,
  FULFILLED,
  CLOSED,
}

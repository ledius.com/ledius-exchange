import { Injectable } from '@angular/core';
import {BehaviorSubject, lastValueFrom, map, tap} from "rxjs";
import {Offer} from "../model/offer/offer";
import {HttpClient, HttpParams} from "@angular/common/http";
import {plainToInstance} from "class-transformer";
import {OfferType} from "../model/offer/offer-type";
import {OfferStatus} from "../model/offer/offer-status";
import {Pagination} from "../model/pagination";
import {BankDetails} from "../model/bank-details/bank-details";

@Injectable({
  providedIn: 'root'
})
export class OffersService {

  public readonly $sellOffers: BehaviorSubject<Offer[]> = new BehaviorSubject<Offer[]>([]);
  public readonly $sellPagination: BehaviorSubject<Pagination> = new BehaviorSubject<Pagination>(Pagination.default());

  public readonly $buyOffers: BehaviorSubject<Offer[]> = new BehaviorSubject<Offer[]>([]);
  public readonly $buyPagination: BehaviorSubject<Pagination> = new BehaviorSubject<Pagination>(Pagination.default());

  constructor(
    private readonly http: HttpClient,
  ) {}

  public async payOffer(offer: Offer, reservedBy: string): Promise<{ paymentUrl: string }> {
    return await lastValueFrom(
      this.http.patch<{ paymentUrl: string }>('/api/ledius-token/offers/pay', { id: offer.id, reservedBy })
        .pipe(
          tap((result) => window.open(result.paymentUrl, '_blank'))
        )
    )
  }

  public async fetchBuyOffers(paginationRequest?: Pagination): Promise<{ items: Offer[], pagination: Pagination }> {
    const { items, pagination } = await lastValueFrom(
      this.http.get<{ items: Offer[], pagination: Pagination }>(`/api/ledius-token/offers`, {
        params: new HttpParams({
          fromObject: {
            type: OfferType.BUY,
            status: OfferStatus.OPENED,
            ...(paginationRequest ? paginationRequest.toObject() : this.$buyPagination.getValue().toObject()),
          },
        })
      })
        .pipe(
          map(({items, pagination}) => ({
            items: plainToInstance(Offer, items),
            pagination: plainToInstance(Pagination, pagination)
          }))
        )
    );

    this.$buyOffers.next(items);
    this.$buyPagination.next(pagination);

    return { items, pagination };
  }

  public async fetchSellOffers(paginationRequest?: Pagination): Promise<{ items: Offer[], pagination: Pagination }> {
    const { items, pagination } = await lastValueFrom(
      this.http.get<{ items: Offer[], pagination: Pagination }>(`/api/ledius-token/offers`, {
        params: new HttpParams({
          fromObject: {
            type: OfferType.SELL,
            status: OfferStatus.OPENED,
            ...(paginationRequest ? paginationRequest.toObject() : this.$buyPagination.getValue().toObject()),
          },
        })
      })
        .pipe(
          map(({items, pagination}) => ({
            items: plainToInstance(Offer, items),
            pagination: plainToInstance(Pagination, pagination)
          }))
        )
    );

    this.$sellOffers.next(items);
    this.$sellPagination.next(pagination);

    return { items, pagination };

  }

  public async getOfferById(id: string): Promise<Offer> {
    const offer = await lastValueFrom(this.http.get(`/api/ledius-token/offers/${id}`));

    return plainToInstance(Offer, offer);
  }

  public async reserveOffer(offer: Offer, bankDetails: BankDetails): Promise<Offer> {
    const reservedOffer = await lastValueFrom(this.http.patch<Offer>('/api/ledius-token/offers/reserve', {
      id: offer.id,
      reservedBy: bankDetails.userId,
      bankDetails: bankDetails.id,
    }));

    return plainToInstance(Offer, reservedOffer);
  }
}

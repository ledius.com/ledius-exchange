import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, lastValueFrom, map, tap} from "rxjs";
import {Account} from "../model/account/account";
import {plainToInstance} from "class-transformer";
import {AuthService} from "./auth.service";
import {Balance} from "../model/account/balance";

@Injectable({
  providedIn: 'root',
})
export class AccountService {

  public readonly $userAccount: BehaviorSubject<Account | null> = new BehaviorSubject<Account | null>(null);
  public readonly $userBalances: BehaviorSubject<Balance[]> = new BehaviorSubject<Balance[]>([]);

  constructor(
    private readonly authService: AuthService,
    private readonly http: HttpClient
  ) {
    this.authService.$authInfo.subscribe(async authInfo => {
      if(!authInfo) return;

      await this.ensureForUser(authInfo.id)
    });

    this.$userAccount.subscribe(async account => {
      if(!account) return;

      this.$userBalances.next(await this.fetchBalances(account.address));
    })
  }

  public async ensureForUser(userId: string): Promise<Account> {
    return await lastValueFrom(
      this.http.post<Account>('/api/ledius-token/accounts', { userId })
        .pipe(
          map(account => plainToInstance(Account, account)),
          tap(account => this.$userAccount.next(account))
        )
    )
  }

  public async fetchBalances(address: string): Promise<Balance[]> {
    return await lastValueFrom(
      this.http.get<Balance[]>(`/api/ledius-token/balance/account?address=${address}`)
        .pipe(
          map(balances => plainToInstance(Balance, balances))
        )
    )
  }

  public async refreshBalances(): Promise<void> {
    const account = await this.$userAccount.getValue();
    if(!account) {
      return;
    }

    const balances = await this.fetchBalances(account.address);
    this.$userBalances.next(balances);
  }
}

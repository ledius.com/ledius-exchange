import { Injectable } from '@angular/core';
import {BehaviorSubject, lastValueFrom, map, tap} from "rxjs";
import {BankDetails} from "../model/bank-details/bank-details";
import {HttpClient, HttpParams} from "@angular/common/http";
import {AuthService} from "./auth.service";
import {plainToInstance} from "class-transformer";

@Injectable({
  providedIn: 'root'
})
export class BankDetailsService {

  public readonly $userBankDetails: BehaviorSubject<BankDetails[]> = new BehaviorSubject<BankDetails[]>([]);

  constructor(
    private readonly authService: AuthService,
    private readonly http: HttpClient,
  ) {
    this.authService.$authInfo.subscribe(async authInfo => {
      if(!authInfo) {
        return;
      }

      await this.fetchUserBankDetails(authInfo.id);
    })
  }

  public async ensureBankDetails(
    payload: {
      userId?: string,
      card: string,
      bank: string,
      firstName: string,
      lastName: string,
      patronymic: string
    }
  ): Promise<BankDetails> {
    payload.userId = this.authService.$authInfo.getValue()?.id;
    return await lastValueFrom(
      this.http.post('/api/ledius-token/banks-details/ensure', payload)
        .pipe(
          map(bankDetails => plainToInstance(BankDetails, bankDetails)),
          tap(() => this.fetchUserBankDetails(payload.userId as string))
        )
    )
  }

  public async fetchUserBankDetails(userId: string): Promise<BankDetails[]> {
    return await lastValueFrom(
      this.http.get<unknown[]>('/api/ledius-token/banks-details', {
        params: new HttpParams().set('userId', userId)
      })
        .pipe(
          map(bankDetails => plainToInstance(BankDetails, bankDetails)),
          tap(bankDetails => this.$userBankDetails.next(bankDetails))
        )
    )
  }
}

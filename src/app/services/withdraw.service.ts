import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {lastValueFrom} from "rxjs";
import {AccountService} from "./account.service";

@Injectable({
  providedIn: 'root'
})
export class WithdrawService {

  constructor(
    private readonly http: HttpClient,
    private readonly accountService: AccountService
  ) {}

  public async withdraw(recipient: string, amount: bigint): Promise<unknown> {
    return await lastValueFrom(
      this.http.post('/api/ledius-token/transfer', {
        sender: this.accountService.$userAccount.getValue()?.userId,
        recipient: recipient,
        amount: String(amount)
      })
    )
  }
}

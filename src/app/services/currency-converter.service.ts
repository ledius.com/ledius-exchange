import { Injectable } from '@angular/core';
import {RateService} from "./rate.service";

@Injectable({
  providedIn: 'root'
})
export class CurrencyConverterService {

  constructor(
    private readonly rateService: RateService
  ) {}

  public async convert(symbol: string = 'LDS', currency: string = 'RUB', amount: bigint): Promise<number> {
    const rate = await this.rateService.getRate(symbol, currency);
    return Number(amount) / (10 ** 18) * rate;
  }
}

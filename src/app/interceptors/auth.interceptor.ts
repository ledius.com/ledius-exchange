import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {AuthService} from "../services/auth.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    protected readonly authService: AuthService
  ) {}

  public intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const authToken = this.authService.$authToken.getValue();

    return next.handle(request.clone({
      setHeaders: {
        Authorization: authToken ? `${authToken.type} ${authToken.accessToken}` : '',
      }
    }));
  }
}

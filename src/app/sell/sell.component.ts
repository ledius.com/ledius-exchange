import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Offer} from "../model/offer/offer";
import {BankDetails} from "../model/bank-details/bank-details";
import {BankDetailsService} from "../services/bank-details.service";
import {OffersService} from "../services/offers.service";
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-sell',
  templateUrl: './sell.component.html',
  styleUrls: ['./sell.component.scss']
})
export class SellComponent implements OnInit {

  public offer?: Offer;
  public bankDetails: BankDetails[] = [];
  public selectedBankDetails?: BankDetails;
  public status: 'fail' | 'success' | undefined = undefined

  constructor(
    private readonly route: ActivatedRoute,
    private readonly bankDetailsService: BankDetailsService,
    private readonly offersService: OffersService,
  ) {
    this.offer = route.snapshot.data['offer'];
    this.bankDetailsService.$userBankDetails.subscribe(bankDetails => {
      this.bankDetails = bankDetails
      this.selectedBankDetails = bankDetails[0];
    });
  }

  ngOnInit(): void {}

  public async handleSubmit(e: Event): Promise<void> {
    e.preventDefault();
    if(!this.selectedBankDetails || !this.offer) {
      return;
    }
    try {
      this.status = undefined;
      await this.offersService.reserveOffer(this.offer, this.selectedBankDetails);
    } catch (e) {
      this.status = 'fail';
    } finally {
      this.status = 'success';
    }
  }

}

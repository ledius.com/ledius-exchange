import { Component, OnInit } from '@angular/core';
import {Balance} from "../model/account/balance";
import {AccountService} from "../services/account.service";
import {OffersService} from "../services/offers.service";
import {Offer} from "../model/offer/offer";
import {Pagination} from "../model/pagination";
import {Router} from "@angular/router";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  public feeOil: Balance = Balance.empty('BNB');
  public balance: Balance = Balance.empty('LDS');

  public buyPagination: Pagination = Pagination.default();
  public sellPagination: Pagination = Pagination.default();

  public offersToBuy: Offer[] = [];
  public offersToSell: Offer[] = [];

  constructor(
    private readonly accountService: AccountService,
    private readonly offersService: OffersService,
    private readonly router: Router,
  ) {
    this.accountService.$userBalances.subscribe((balances) => {
      balances.filter(balance => balance.isLDS()).forEach(balance => this.balance = balance);
      balances.filter(balance => balance.isBNB()).forEach(balance => this.feeOil = balance);
    });

    this.offersService.$buyOffers.subscribe(offers => this.offersToSell = offers);
    this.offersService.$sellOffers.subscribe(offers => this.offersToBuy = offers);
    this.offersService.$buyPagination.subscribe(pagination => {
      this.buyPagination = pagination;
    });
    this.offersService.$sellPagination.subscribe(pagination => {
      this.sellPagination = pagination;
    })
  }

  public async ngOnInit(): Promise<void> {
    await Promise.all([
      this.offersService.fetchBuyOffers(),
      this.offersService.fetchSellOffers(),
    ]);
  }

  public async buyOffer(offer: Offer): Promise<void> {
    await this.offersService.payOffer(offer, this.accountService.$userAccount.getValue()?.userId as string);
  }

  public async sellOffer(offer: Offer): Promise<void> {
    await this.router.navigateByUrl(`/sell/${offer.id}`);
  }

  public async nextToBuyPage(): Promise<void> {
    await this.offersService.fetchSellOffers(this.sellPagination.nextPage());
  }

  public async prevToBuyPage(): Promise<void> {
    await this.offersService.fetchSellOffers(this.sellPagination.prevPage());
  }

  public async nextToSellPage(): Promise<void> {
    await this.offersService.fetchBuyOffers(this.buyPagination.nextPage());
  }

  public async prevToSellPage(): Promise<void> {
    await this.offersService.fetchBuyOffers(this.buyPagination.prevPage());

  }
}

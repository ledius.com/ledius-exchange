import { Component, Input, OnInit } from '@angular/core';
import {Balance} from "../model/account/balance";
import {AccountService} from "../services/account.service";
import {WithdrawService} from "../services/withdraw.service";

@Component({
  selector: 'app-withdrawal',
  templateUrl: './withdrawal.component.html',
  styleUrls: ['./withdrawal.component.scss']
})
export class WithdrawalComponent {

  public balance: Balance = Balance.empty('LDS');
  public amount: string = "0";
  public recipient: string = "";
  public inProgress: boolean = false;
  public failed: boolean = false;
  public success: boolean = false;

  constructor(
    private readonly accountService: AccountService,
    private readonly withdrawService: WithdrawService
  ) {
    this.accountService.$userBalances.subscribe(balances => {
      const ldsBalance = balances.find(balance => balance.isLDS());

      this.balance = ldsBalance ?? Balance.empty('LDS');
      this.amount = this.balance.formattedBalance(18);
    });
  }

  public async withdraw(): Promise<void> {
    if(this.inProgress) {
      return;
    }
    try {
      this.inProgress = true;
      this.failed = false;
      this.success = false;
      const amount = BigInt(BigInt(Number(this.amount)) * BigInt(10 ** 18));
      const recipient = this.recipient;

      await this.withdrawService.withdraw(recipient, amount);
      this.success = true;
    } catch (e) {
      this.failed = true;
    }
    finally {
      this.inProgress = false;
    }


  }

}

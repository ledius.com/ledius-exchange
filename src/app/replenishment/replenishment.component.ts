import { Component } from '@angular/core';
import {BankDetailsService} from "../services/bank-details.service";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-replenishment',
  templateUrl: './replenishment.component.html',
  styleUrls: ['./replenishment.component.scss']
})
export class ReplenishmentComponent {
  selectedBank = 1
  banks = [
    { id: 1, name: 'Тинькофф' },
    { id: 2, name: 'СберБанк' },
    { id: 3, name: 'АльфаБанк'},
    { id: 4, name: 'ВТБ'},
  ];

  public readonly form = new FormGroup({
    card: new FormControl(''),
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    patronymic: new FormControl(''),
    bank: new FormControl('')
  })

  constructor(
    private readonly bankDetailsService: BankDetailsService
  ) { }

  public async ensure(): Promise<void> {
    await this.bankDetailsService.ensureBankDetails(this.form.getRawValue());
  }

}

export interface NavbarItem {
  readonly icon: string;
  readonly title: string;
  readonly route: string;
  readonly disabled?: boolean;
}

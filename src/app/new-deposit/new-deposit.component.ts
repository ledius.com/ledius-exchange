import { Component, OnInit } from '@angular/core';
import {LendingService} from "../services/lending.service";
import {AccountService} from "../services/account.service";
import {Balance} from "../model/account/balance";
import {FormControl, FormGroup} from "@angular/forms";
import { addDays } from "date-fns";

@Component({
  selector: 'app-new-deposit',
  templateUrl: './new-deposit.component.html',
  styleUrls: ['./new-deposit.component.scss']
})
export class NewDepositComponent implements OnInit {
  public balance = Balance.empty('LDS');
  public form = new FormGroup({
    amount: new FormControl(0),
    duration: new FormControl(30),
  });
  public endDate?: Date;
  public expectedAmount: number = 0;
  public createInProgress: boolean = false;


  status = {type: '', text: ''}
  statuses = [
    {type: 'success', text: 'Депозит успешно создан'},
    {type: 'fail', text: 'Депозит не создан'}
  ]
  periods = [
    { value: 30},
    { value: 60},
    { value: 90},
    { value: 120},
    { value: 365},
  ]
  constructor(
    private readonly lendingService: LendingService,
    private readonly accountService: AccountService,
  ) {
    this.accountService.$userBalances.subscribe(balances => {
      const ldsBalance = balances.find(balance => balance.isLDS());
      if(ldsBalance) {
        this.balance = ldsBalance;
        this.form.patchValue({
          amount: ldsBalance.formattedBalance(),
        })
      }
    });
    this.form.valueChanges.subscribe(val => {
      this.endDate = addDays(new Date(), val.duration);
      this.expectedAmount = this.calcProfit(Number(val.amount), val.duration || this.periods[0].value);
      console.log(val, this.endDate, this.expectedAmount, this.expectedAmount / Number(val.amount));
    });
  }

  public calcProfit(amount: number, duration: number): number {
    return Number(amount) * (1 + 0.01 * (duration / 10))
  }

  ngOnInit(): void {
  }

  public async handleSubmit(): Promise<void> {
    if(this.createInProgress) {
      return;
    }

    try {
      this.createInProgress = true;
      const payload = {
        amount: String((BigInt(Number(this.form.get('amount')?.value)) * BigInt(10 ** 18))),
        endDate: this.endDate as Date,
        userId: this.accountService.$userAccount.getValue()?.userId as string
      };

      await this.lendingService.create(payload);
      await this.accountService.refreshBalances();

      this.status = this.statuses[0];
    } catch (e) {
      this.status = this.statuses[1];
    }
    finally {
      this.createInProgress = false;
    }


  }
}

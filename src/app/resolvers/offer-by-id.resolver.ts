import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import {OffersService} from "../services/offers.service";
import {Offer} from "../model/offer/offer";

@Injectable({
  providedIn: 'root'
})
export class OfferByIdResolver implements Resolve<Offer | undefined> {

  constructor(
    private readonly offers: OffersService
  ) {
  }

  public async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Offer | undefined> {
    const offerId = route.params['offerId'];

    if(offerId) {
      return await this.offers.getOfferById(offerId);
    }
    return undefined;
  }
}

import { TestBed } from '@angular/core/testing';

import { OfferByIdResolver } from './offer-by-id.resolver';

describe('OfferByIdResolver', () => {
  let resolver: OfferByIdResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(OfferByIdResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
